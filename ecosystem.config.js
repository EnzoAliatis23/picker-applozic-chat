module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    {
      name: "picker-applozic-chat",
      script: "app.js",
      exec_mode: "cluster",
    },
  ],
};
